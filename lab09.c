#include<stdio.h>
#include<stdlib.h>


typedef void (*vmethod)(void* this);
vmethod *vtable;
vmethod *vtable2;

typedef struct _MyMatrix {
	int rows;
	int columns;
	int *matrixarray;
	void (*add)(void* this, void* matrix2);
	void (*mult)(void* this, void* matrix2);
	vmethod *vtable_ptr;

} MyMatrix;

typedef struct MyVector_ {
	MyMatrix a;
	int minimum;
	int maximum;
	vmethod *vtable_ptr;
} MyVector;

void MNormMethod( MyMatrix* this) {
	int max=0;
	int norm=0;
	int i=0;
	int j=0;

	for(i=0; i<1; i++){
		for(j=0; j<this->rows;j++){
  			max +=this->matrixarray[j*this->columns+i];
		}
	} 
	norm=max;
	max=0;
	for(i=0; i<this->columns; i++){
		for(j=0; j<this->rows;j++){
  			max +=this->matrixarray[j*this->columns+i];
	}
		if(max>norm){
		norm=max;	
	}
	max=0;
	} 
	
	printf("Matrix Norm: %d\n",norm);
}

void VNormMethod( MyMatrix* this){
	int max=0;
	int i=0;
	int j=0;

	for(j=0; j<this->rows;j++){
		max+=this->matrixarray[i*this->rows+j];
	}
	printf("Vector Norm: %d\n", max);
	}

void AddMethod( MyMatrix* this, MyMatrix* b) {
	if((this->rows==b->rows) && (this->columns==b->columns)){
	int i=0;
	int j=0;
	printf("Sum:\n");
	for(i=0; i<this->rows; i++){
		for(j=0; j<this->columns;j++){
  			this->matrixarray[i*this->columns+j]=this->matrixarray[i*this->columns+j]+ b->matrixarray[i*this->columns+j];
  			printf("%d\t", this->matrixarray[i*this->columns+j]);
		}
  		printf("\n");
	} 
	}
	else{
   		printf("Addition cannot be performed.\n");
	}

}

void VAddMethod( MyMatrix* this, MyMatrix* b) {
	if((this->columns==b->columns)){
		int i=0;
		int j=0;
		printf("Sum:\n");
		for(j=0; j<this->rows;j++){
  			this->matrixarray[i*this->rows+j]=this->matrixarray[i*this->rows+j]+ b->matrixarray[i*this->rows+j];
  			printf("%d\n", this->matrixarray[i*this->rows+j]);
		}
  	 	printf("\n");
 	}
	else{
   		printf("Addition cannot be performed.\n");
	}
}

void MultMethod( MyMatrix* this, MyMatrix* b) { 
	if (this->columns == b->rows){
		printf("Resultant\n");
		int i=0;
		int j=0;
		int k=0;
		int temp=0;
	
		for (i=0; i<this->rows; i++){
			for(j=0; j<b->columns; j++){
				for(k=0; k<this->columns; k++){
			
					temp += this->matrixarray[i*this->columns+k] * b->matrixarray[k*b->columns+j];
				}
			
				printf("%d \t", temp);
				temp=0;
			
			}
			printf("\n");
		}
	}
	else{
		printf("Multiplication cannot be performed.\n");
	}

}

void init_myMatrix(MyMatrix* this, int rows, int columns){
	this->add = &AddMethod;
	this->mult = &MultMethod;
	this->rows=rows;
	this->columns=columns;
	this->matrixarray= malloc(sizeof(int)*rows*columns);
	int i=0;
	int j=0;
	printf("Matrix:\n");
	for(i=0; i<rows; i++){
		for(j=0; j<columns;j++){
			this->matrixarray[i*this->columns+j]=j+1;
			printf("%d\t", this->matrixarray[i*this->columns+j]);
		}
 		printf("\n");
	}

}

void init_myVector(MyVector* this, int rows){
	this->a.add = &VAddMethod;
	this->a.mult = &MultMethod;
	this->a.rows=rows;
	this->a.columns=1;
	this->a.matrixarray= malloc(sizeof(int)*rows*this->a.columns);
	int i=0;
	int j=0;
	int min=0;
	int max=0;
	printf("\nVector:\n");
	for(j=0; j<rows;j++){
		this->a.matrixarray[i*this->a.rows+j]=j+1;
		printf("%d\n", this->a.matrixarray[i*this->a.rows+j]);
		if(this->a.matrixarray[i*this->a.rows+j]>max){
			max=this->a.matrixarray[i*this->a.rows+j];
		}
	}
 	
	printf("\n");
	min=this->a.matrixarray[0];
	
	for(j=0; j<rows;j++){
		if(this->a.matrixarray[i*this->a.rows+j]<min){
			min=this->a.matrixarray[i*this->a.rows+j];
		}
	}
	printf("Max: %d\n",max);
	printf("Min: %d\n",min);

}


int main(){

	vtable=(vmethod*)malloc(sizeof(vmethod)* 20);
	vtable2=(vmethod*)malloc(sizeof(vmethod)* 20);
	vtable[0]=&MNormMethod;
	vtable2[0]=&VNormMethod;

	int i=0;
	int a=0;
	printf("Press 1 for Matrix, 2 for Vector\n");
	scanf("%d",&i);
	if(i==1){
		printf("-1 for Addition \n-2 for Multiplication\n-3 for Norm\n");
		scanf("%d", &a);
		if(a==1){
			MyMatrix A;
			init_myMatrix(&A, 2,2);
			MyMatrix B;
			init_myMatrix(&B, 2,2);
			A.add(&A, &B);
		}	
		else if(a==2){
			MyMatrix A;
			init_myMatrix(&A, 2,2);
			MyMatrix B;
			init_myMatrix(&B, 2,2);
			A.mult(&A, &B);
		}
		else if(a==3){
			MyMatrix A;
			init_myMatrix(&A, 2,2);
			A.vtable_ptr=vtable;
			A.vtable_ptr[0](&A);
		}
		else{
			printf("Invalid input\n");
		}
	}
	else if(i==2){
		printf("-1 for Addition \n-2 for Norm\n");
		scanf("%d",&a);
		if(a==1){
			MyVector V;
			init_myVector(&V,3);
			MyVector V2;
			init_myVector(&V2,3);
			V.a.add(&V,&V2);
		}
		else if(a==2){
			MyVector V;
			init_myVector(&V,3);
			V.vtable_ptr=vtable2;
			V.vtable_ptr[0](&V);
		}
		else {
			printf("Invalid input\n");
		}
	}
		
	return 0;
}

